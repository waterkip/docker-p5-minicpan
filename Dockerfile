FROM perl:latest
LABEL maintainer="Wesley Schwengle <wesleys@schwengle.net>"

USER root
WORKDIR /tmp/build

COPY cpanfile .

RUN cpanm --installdeps . \
    && rm -rf /root/.cpanm

WORKDIR /opt/cpan

COPY minicpanrc /opt/cpan/.minicpanrc

ENTRYPOINT [ "/bin/bash", "-c" ]
CMD [ "minicpan -C /opt/cpan/.minicpanrc -d 0755" ]
