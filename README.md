# Welcome to docker-p5-minicpan

## Installation

### TL;DR;

```
git clone git@gitlab.com:waterkip/docker-p5-minicpan
cd docker-cpan

# Create the needed elements
docker network create minicpan
docker volume create minicpan

# Build and do things
docker-compose build minicpan
docker-compose up --no-start
docker-compose start
```

# Reuse elsewhere

You can use the mirror from other projects

```
version: "3.6"

services:
  perlthings:
    build:
      context: .
      dockerfile: Dockerfile
      # Get to build from your own mirror
      network: minicpan
    networks:
      default:
      # Get to build inside your container from the mirror
      minicpan:

networks:
  default:
  minicpan:
   external: true
```

Enjoy the ride

# Credits

Some files are taken from https://github.com/colinnewell/CPAN-Mirror-Docker,
mainly the nginx side of things. His repo is published under an MIT license as
well. See the LICENSE file for more information
